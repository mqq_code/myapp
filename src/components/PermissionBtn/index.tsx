import React from 'react';
import { Button } from 'antd';
import type { ButtonProps } from 'antd';
import { useModel } from 'umi';

interface IProps {
  name: string; // 次按钮的权限名称
}

const Index: React.FC<IProps & ButtonProps> = (props) => {
  const { initialState } = useModel('@@initialState');
  // 查找当前用户是否存在此按钮权限
  if (initialState?.currentUser?.permission.find((v) => v.path === props.name)) {
    return <Button {...props} />;
  }
  return null;
};

export default Index;
