import React from 'react';
import { LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Menu } from 'antd';
import { history, useModel } from 'umi';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import type { MenuInfo } from 'rc-menu/lib/interface';
import { logout } from '@/services/user';

export type GlobalHeaderRightProps = {
  menu?: boolean;
};

const AvatarDropdown: React.FC<GlobalHeaderRightProps> = ({ menu }) => {
  // 获取个人信息
  const { initialState, setInitialState } = useModel('@@initialState');

  const onMenuClick = async (event: MenuInfo) => {
    const { key } = event;
    if (key === 'logout') {
      await logout();
      setInitialState({ ...initialState!, currentUser: undefined });
      localStorage.removeItem('token');
      history.replace({
        pathname: '/login',
      });
      return;
    }
  };

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      {menu && (
        <Menu.Item key="center">
          <UserOutlined />
          个人中心
        </Menu.Item>
      )}
      {menu && <Menu.Divider />}

      <Menu.Item key="logout">
        <LogoutOutlined />
        退出登录
      </Menu.Item>
    </Menu>
  );
  return (
    <HeaderDropdown overlay={menuHeaderDropdown}>
      <span className={`${styles.action} ${styles.account}`}>
        <Avatar className={styles.avatar} src={initialState?.currentUser?.avator} alt="avatar">
          {initialState?.currentUser?.username}
        </Avatar>
        <span className={`${styles.name} anticon`}>{initialState?.currentUser?.username}</span>
      </span>
    </HeaderDropdown>
  );
};

export default AvatarDropdown;
