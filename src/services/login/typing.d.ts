declare namespace LoginAPI {
  interface LoginParams {
    code: string;
    password: string;
    username: string;
  }
  type captchaRes = BaseResponse<{
    code: string;
  }>;
  type loginRes = BaseResponse<{
    token: string;
  }>;
}
