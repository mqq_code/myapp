import request from '../request';

export const getCaptcha = () => {
  return request<LoginAPI.captchaRes>('/login/captcha');
};

export const login = (data: LoginAPI.LoginParams) => {
  return request<LoginAPI.loginRes>('/login', {
    method: 'post',
    data,
  });
};
