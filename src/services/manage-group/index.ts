import request from '../request';

// 班级列表
export const getStudentGroup = (params: ManageAPI.StudentGroupParams) => {
  return request.get<ManageAPI.StudentGroupRes>('/studentGroup/list', {
    params,
  });
};

// 新增班级
export const createStudentGroup = (data: ManageAPI.CreateGroupParams) => {
  return request.post<BaseResponse>('/studentGroup/create', {
    data,
  });
};
// 删除班级
export const delStudentGroup = (id: string) => {
  return request.post<BaseResponse>('/studentGroup/remove', {
    data: {
      id,
    },
  });
};
// 获取科目列表
export const getClassifyList = (params: { page?: number; pagesize?: number } = {}) => {
  return request.get<ManageAPI.ClassifyListRes>('/classify/list', {
    params,
  });
};

// 学生列表
export const getStudentList = (params: ManageAPI.StudentListParams) => {
  return request.get<ManageAPI.StudentListRes>('/student/list', {
    params,
  });
};
// 新增学生
export const createStudent = (data: ManageAPI.CreateStudentParams) => {
  return request.post<BaseResponse>('/student/create', {
    data,
  });
};
// 删除学生
export const delStudent = (id: string) => {
  return request.post<BaseResponse>('/student/remove', {
    data: {
      id,
    },
  });
};
