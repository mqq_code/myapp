declare namespace ManageAPI {
  // 班级列表接口
  interface StudentGroupParams {
    page: number;
    pagesize: number;
    name?: string;
    classify?: string;
  }
  interface StudentItem {
    age: number;
    avator: string;
    classId: string;
    className: string;
    createTime: number;
    creator: string;
    email: string;
    exams: [];
    password: string;
    role: string;
    sex: string;
    status: number;
    username: string;
    _id: string;
  }
  interface StudentGroupItem {
    classify: string;
    createTime: number;
    creator: string;
    name: string;
    students: StudentItem[];
    teacher: string;
    _id: string;
  }
  type StudentGroupRes = BaseResponse<{
    list: StudentGroupItem[];
    total: number;
    totalPage: number;
  }>;
  // 新增班级
  interface CreateGroupParams {
    name: string;
    classify: string;
    teacher: string;
    students?: string[];
  }
  // 科目列表接口
  interface ClassifyListItem {
    _id: string;
    name: string;
    value: string;
    creator: string;
    createTime: number;
  }
  type ClassifyListRes = BaseResponse<{
    total: number;
    page: number;
    pagesize: number;
    totalPage: number;
    list: ClassifyListItem[];
  }>;

  // 学生列表接口
  interface StudentListParams {
    page?: number;
    pagesize?: number;
    username?: string;
    sex?: '男' | '女';
    age?: number;
    classId?: string;
  }
  interface StudentItem {
    _id: string;
    username: string;
    password: string;
    sex: '男' | '女';
    age: number;
    email: string;
    className: string;
    avator: string;
    status: 0 | 1;
    creator: string;
    createTime: number;
    role: string;
  }
  type StudentListRes = BaseResponse<{
    list: StudentItem[];
    total: number;
    totalPage: number;
  }>;
  // 创建学生
  interface CreateStudentParams {
    username: string;
    password: string;
    sex: string;
    age: 20;
    email: string;
    className: string;
    avator?: string;
    status?: 0 | 1;
  }
}
