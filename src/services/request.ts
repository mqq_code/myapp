import { extend } from 'umi-request';
import type { RequestOptionsInit } from 'umi-request';
import { message } from 'antd';
import { history } from 'umi';

// 根据不同的开发环境,配置请求前缀
interface ApiPrefix {
  development: string;
  production: string;
}
export const apiPreFix: ApiPrefix = {
  development: '/api',
  production: '//zyxcl.xyz/exam_api',
};

export const baseUrl = apiPreFix[process.env.NODE_ENV!];
/**
 * 配置request请求时的默认参数
 */
const request = extend({
  prefix: baseUrl, // 根据环境配置不同的请求前缀
  credentials: 'same-origin', // 发起请求时把 cookie 自动传给接口
});

// request拦截器, 携带token
request.interceptors.request.use((url: string, options: RequestOptionsInit) => {
  // 不携带token的请求数组
  const notCarryTokenArr: string[] = ['/login', '/login/captcha'];
  if (notCarryTokenArr.includes(url)) {
    return {
      url,
      options,
    };
  }
  // 请求接口时自动把token传给所有接口的 headers
  const token = localStorage.getItem('token') || '';
  return {
    url,
    options: {
      ...options,
      headers: {
        ...options.headers,
        Authorization: token,
      },
    },
  };
});

const codeMessage = {
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '登陆信息失效，请重新登陆',
  403: '没有请求权限，请联系管理员',
  500: '服务器发生错误，请检查服务器。',
};
// 响应拦截器，提前对响应做异常处理
request.interceptors.response.use(async (response) => {
  if (response.status !== 200) {
    const errorText = codeMessage[response.status] || response.statusText;
    message.error(errorText);
    if (response.status === 401) {
      history.replace('/login?redirect=' + encodeURIComponent(history.location.pathname));
    }
    throw new Error(response.status + response.statusText);
  }
  return response;
});

export default request;
