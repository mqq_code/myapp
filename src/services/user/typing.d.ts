declare namespace UserAPI {
  // userlist 接口
  interface UserlistParams {
    page?: number;
    pagesize?: number;
  }
  interface UserlistItem {
    _id: string;
    username: string;
    password: string;
    status: 0 | 1;
  }
  type UserlistRes = BaseResponse<{
    total: number;
    page: number;
    pagesize: number;
    totalPage: number;
    list: UserlistItem[];
  }>;
  // 个人信息
  interface PermissionItem {
    disabled: boolean;
    isBtn: boolean;
    name: string;
    path: string;
    pid: string;
    _id: string;
  }
  type UserInfoRes = {
    username: string;
    avator?: string;
    email?: string;
    role: string[];
    permission: PermissionItem[];
  };
  // 左侧菜单列表
  interface MenuItem {
    _id: string;
    name: string;
    path: string;
    disabled: boolean;
    createTime: number;
    isBtn: boolean;
    children?: MenuItem[];
  }
  interface UserMenu {
    list: MenuItem[];
  }
}
