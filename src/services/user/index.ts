import request from '../request';

export const getUserlist = (params: UserAPI.UserlistParams = {}) => {
  return request.get<UserAPI.UserlistRes>('/user/list', {
    params,
  });
};

export const getUserInfo = () => {
  return request.get<BaseResponse<UserAPI.UserInfoRes>>('/user/info');
};

export const logout = () => {
  return request.post<BaseResponse>('/user/logout');
};

export const getUserMenu = () => {
  return request.get<BaseResponse<UserAPI.UserMenu>>('/user/menulist');
};
