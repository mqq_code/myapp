import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  getStudentGroup,
  getStudentList,
  delStudent,
  createStudent,
} from '@/services/manage-group';
import { PageContainer } from '@ant-design/pro-layout';
import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns, ProFormInstance } from '@ant-design/pro-components';
import {
  ProForm,
  ProTable,
  DrawerForm,
  ProFormSelect,
  ProFormText,
} from '@ant-design/pro-components';
import { Button, Popconfirm, Space, Tag, message } from 'antd';
import moment from 'moment';
import { useRequest } from 'umi';

const Index = () => {
  const actionRef = useRef<ActionType>();
  // 新建表单弹窗
  const [drawerVisit, setDrawerVisit] = useState(false);
  const restFormRef = useRef<ProFormInstance>();
  useEffect(() => {
    if (!drawerVisit) {
      restFormRef.current?.resetFields();
    }
  }, [drawerVisit]);
  // 分页对象
  const [listparams, setListparams] = useState<ManageAPI.StudentGroupParams>({
    page: 1,
    pagesize: 5,
  });
  // 获取班级列表
  const { data: classNamesData } = useRequest(getStudentGroup);
  const classNamesOptions = useMemo(() => {
    return classNamesData?.data.list.map((v) => ({ label: v.name, value: v._id })) || [];
  }, [classNamesData]);
  // 删除班级
  const delRow = async (id: string) => {
    const res = await delStudent(id);
    if (res.code === 200) {
      message.success('删除成功');
      actionRef.current?.reload();
    } else {
      message.error(res.msg);
    }
  };
  const columns: ProColumns<ManageAPI.StudentItem>[] = [
    {
      title: '姓名',
      dataIndex: 'username',
    },
    {
      title: '性别',
      dataIndex: 'sex',
      valueType: 'select',
      fieldProps: {
        options: [
          { label: '男', value: '男' },
          { label: '女', value: '女' },
        ],
      },
      render: (_) => {
        return <Tag color="blue">{_}</Tag>;
      },
    },
    {
      title: '年龄',
      dataIndex: 'age',
    },
    {
      title: '班级',
      dataIndex: 'className',
      valueType: 'select',
      fieldProps: {
        options: classNamesOptions,
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      search: false,
      render: (_, record) => {
        return moment(record.createTime).format('YYYY-MM-DD kk:mm:ss');
      },
    },
    {
      title: '操作',
      valueType: 'option',
      key: 'option',
      render: (text, record, _, action) => {
        return (
          <Space>
            <Button type="link" size="small">
              查看
            </Button>
            <Button type="link" size="small">
              编辑
            </Button>
            <Popconfirm
              title="确定删除此行数据吗"
              onConfirm={() => delRow(record._id)}
              onCancel={() => {
                message.warning('取消删除');
              }}
              okButtonProps={{ danger: true }}
              okText="删除"
              cancelText="取消"
            >
              <Button type="link" danger size="small">
                删除
              </Button>
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <PageContainer>
      <ProTable<ManageAPI.StudentItem>
        columns={columns}
        actionRef={actionRef}
        cardBordered
        request={async ({ current, pageSize, ...other }) => {
          const newparams = {
            page: current || 1,
            pagesize: pageSize || 5,
            ...other,
          };
          setListparams(newparams);
          const res = await getStudentList(newparams);
          return {
            data: res.data.list,
            total: res.data.total,
          };
        }}
        onDataSourceChange={(dataSource) => {
          if (dataSource.length === 0 && listparams.page > 1) {
            setListparams({
              ...listparams,
              page: listparams.page - 1,
            });
          }
        }}
        rowKey="_id"
        search={{
          labelWidth: 80,
        }}
        options={{
          setting: {
            listsHeight: 400,
          },
        }}
        pagination={{
          current: listparams.page,
          pageSize: listparams.pagesize,
          pageSizeOptions: [5, 10, 20, 50],
          showSizeChanger: true,
        }}
        dateFormatter="string"
        headerTitle="班级列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            onClick={() => setDrawerVisit(true)}
            type="primary"
          >
            新建
          </Button>,
        ]}
      />
      <DrawerForm<ManageAPI.CreateStudentParams>
        open={drawerVisit}
        onOpenChange={setDrawerVisit}
        formRef={restFormRef}
        title="新建班级"
        onFinish={async (values) => {
          const res = await createStudent({
            ...values,
            status: 1,
            avator: '',
            password: '123',
          });
          if (res.code === 200) {
            message.success('提交成功');
            actionRef.current?.reload();
            return true;
          } else {
            message.error(res.msg);
            return false;
          }
        }}
      >
        <ProForm.Group>
          <ProFormText
            rules={[{ required: true }]}
            width="md"
            name="username"
            label="姓名"
            placeholder="请输入姓名"
          />
          <ProFormSelect
            rules={[{ required: true }]}
            width="sm"
            name="sex"
            label="性别"
            options={[
              { label: '男', value: '男' },
              { label: '女', value: '女' },
            ]}
          />
          <ProFormText rules={[{ required: true }]} width="xs" name="age" label="年龄" />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormSelect
            rules={[{ required: true }]}
            width="md"
            options={classNamesOptions}
            name="className"
            label="选择班级"
          />
          <ProFormText width="md" name="email" label="邮箱" placeholder="请输入邮箱" />
        </ProForm.Group>
        <ProFormText width="md" name="idcard" label="身份证号" placeholder="请输入身份证号" />
      </DrawerForm>
    </PageContainer>
  );
};

export default Index;
