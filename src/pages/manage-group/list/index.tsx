import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  getStudentGroup,
  delStudentGroup,
  getClassifyList,
  createStudentGroup,
} from '@/services/manage-group';
import { getUserlist } from '@/services/user';
import { PageContainer } from '@ant-design/pro-layout';
import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns, ProFormInstance } from '@ant-design/pro-components';
import { ProTable, DrawerForm, ProFormSelect, ProFormText } from '@ant-design/pro-components';
import { Button, Popconfirm, Space, Tag, message } from 'antd';
import moment from 'moment';
import { useRequest } from 'umi';
import PermissionBtn from '@/components/PermissionBtn';

const Index = () => {
  const actionRef = useRef<ActionType>();
  // 新建表单弹窗
  const [drawerVisit, setDrawerVisit] = useState(false);
  const restFormRef = useRef<ProFormInstance>();
  useEffect(() => {
    if (!drawerVisit) {
      restFormRef.current?.resetFields();
    }
  }, [drawerVisit]);
  // 分页对象
  const [listparams, setListparams] = useState<ManageAPI.StudentGroupParams>({
    page: 1,
    pagesize: 5,
  });
  // 获取科目分类
  const { data: classifyData } = useRequest(getClassifyList);
  const classifyOptions = useMemo(() => {
    return classifyData?.data.list.map((v) => ({ label: v.name, value: v.name })) || [];
  }, [classifyData]);
  // 获取老师列表
  const { data: teacherData } = useRequest(getUserlist);
  const teacherOptions = useMemo(() => {
    return teacherData?.data.list.map((v) => ({ label: v.username, value: v.username })) || [];
  }, [teacherData]);
  // 删除班级
  const delRow = async (row: ManageAPI.StudentGroupItem) => {
    const res = await delStudentGroup(row._id);
    if (res.code === 200) {
      message.success('删除成功');
      actionRef.current?.reload();
    } else {
      message.error(res.msg);
    }
  };
  const columns: ProColumns<ManageAPI.StudentGroupItem>[] = [
    {
      title: '班级名称',
      dataIndex: 'name',
    },
    {
      title: '科目',
      dataIndex: 'classify',
      valueType: 'select',
      fieldProps: {
        options: classifyOptions,
      },
      render: (_) => {
        return <Tag color="blue">{_}</Tag>;
      },
    },
    {
      title: '老师',
      dataIndex: 'teacher',
      valueType: 'select',
      fieldProps: {
        options: teacherOptions,
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      search: false,
      render: (_, record) => {
        return moment(record.createTime).format('YYYY-MM-DD kk:mm:ss');
      },
    },
    {
      title: '操作',
      valueType: 'option',
      key: 'option',
      render: (text, record, _, action) => {
        return (
          <Space>
            <Button type="link" size="small">
              查看
            </Button>
            <Button type="link" size="small">
              编辑
            </Button>
            <Popconfirm
              title="确定删除此行数据吗"
              onConfirm={() => delRow(record)}
              onCancel={() => {
                message.warning('取消删除');
              }}
              okButtonProps={{ danger: true }}
              okText="删除"
              cancelText="取消"
            >
              <PermissionBtn type="link" danger size="small" name="groupDelBtn">
                删除
              </PermissionBtn>
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <PageContainer>
      <ProTable<ManageAPI.StudentGroupItem>
        columns={columns}
        actionRef={actionRef}
        cardBordered
        request={async ({ current, pageSize, ...other }) => {
          const newparams = {
            page: current || 1,
            pagesize: pageSize || 5,
            ...other,
          };
          setListparams(newparams);
          const res = await getStudentGroup(newparams);
          return {
            data: res.data.list,
            total: res.data.total,
          };
        }}
        onDataSourceChange={(dataSource) => {
          if (dataSource.length === 0 && listparams.page > 1) {
            setListparams({
              ...listparams,
              page: listparams.page - 1,
            });
          }
        }}
        rowKey="_id"
        search={{
          labelWidth: 80,
        }}
        options={{
          setting: {
            listsHeight: 400,
          },
        }}
        pagination={{
          current: listparams.page,
          pageSize: listparams.pagesize,
          pageSizeOptions: [5, 10, 20, 50],
          showSizeChanger: true,
        }}
        dateFormatter="string"
        headerTitle="班级列表"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            onClick={() => setDrawerVisit(true)}
            type="primary"
          >
            新建
          </Button>,
        ]}
      />
      <DrawerForm<ManageAPI.CreateGroupParams>
        open={drawerVisit}
        onOpenChange={setDrawerVisit}
        formRef={restFormRef}
        title="新建班级"
        width={500}
        onFinish={async (values) => {
          const res = await createStudentGroup({ ...values, students: [] });
          if (res.code === 200) {
            message.success('提交成功');
            actionRef.current?.reload();
            return true;
          } else {
            message.error(res.msg);
            return false;
          }
        }}
      >
        <ProFormText
          rules={[{ required: true }]}
          width="md"
          name="name"
          label="班级名称"
          placeholder="请输入名称"
        />
        <ProFormSelect
          rules={[{ required: true }]}
          width="md"
          options={classifyOptions}
          name="classify"
          label="选择科目"
        />
        <ProFormSelect
          rules={[{ required: true }]}
          width="md"
          options={teacherOptions}
          name="teacher"
          label="选择老师"
        />
      </DrawerForm>
    </PageContainer>
  );
};

export default Index;
