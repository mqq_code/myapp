import Footer from '@/components/Footer';
import { login, getCaptcha } from '@/services/login';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { LoginForm, ProFormText, ProForm } from '@ant-design/pro-form';
import { message, Input, Row, Col, Tabs } from 'antd';
import { history, useRequest, useModel } from 'umi';
import styles from './index.less';
import { useState } from 'react';

const subTitle = {
  student: '在线考试平台',
  teacher: '考试平台管理后台',
};
const title = {
  student: 'OnlineExam',
  teacher: 'OnlineExamAdmin',
};
const Login: React.FC = () => {
  const { initialState, setInitialState } = useModel('@@initialState');
  const [type, setType] = useState<string>('student');
  const { data, run: refreshCode } = useRequest(getCaptcha);

  const onFinish = async (values: any) => {
    const res = await login(values);
    if (res.code === 200) {
      message.success('登陆成功');
      localStorage.setItem('token', res.data.token);
      // 调用获取用户信息接口，存到全局数据
      const userinfoRes = await initialState!.fetchUserInfo();
      setInitialState({ ...initialState!, currentUser: userinfoRes });
      history.push('/');
    } else if (res.code === 1005) {
      message.error(res.msg);
      refreshCode();
    } else {
      message.error(res.msg);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <LoginForm
          submitter={{
            submitButtonProps: {
              block: true,
              size: 'large',
            },
          }}
          logo="https://github.githubassets.com/images/modules/logos_page/Octocat.png"
          title={title[type]}
          subTitle={subTitle[type]}
          onFinish={onFinish}
        >
          <Tabs
            activeKey={type}
            onChange={setType}
            items={[
              {
                key: 'student',
                label: '我是学生',
              },
              {
                key: 'teacher',
                label: '我是老师',
              },
            ]}
          />
          <ProFormText
            name="username"
            fieldProps={{
              size: 'large',
              prefix: <UserOutlined className={styles.prefixIcon} />,
            }}
            placeholder="请输入用户名"
            rules={[
              {
                required: true,
                message: '请输入用户名！',
              },
            ]}
          />
          <ProFormText.Password
            name="password"
            fieldProps={{
              size: 'large',
              prefix: <LockOutlined className={styles.prefixIcon} />,
            }}
            placeholder="请输入密码"
            rules={[
              {
                required: true,
                message: '请输入密码！',
              },
            ]}
          />
          <ProForm.Item
            name="code"
            rules={[
              {
                required: true,
                message: '请输入验证码！',
              },
            ]}
          >
            <Row gutter={20}>
              <Col span={14}>
                <Input
                  size="large"
                  prefix={<LockOutlined className={styles.prefixIcon} />}
                  placeholder="请输入验证码"
                />
              </Col>
              <Col span={10}>
                <div className={styles.code} onClick={refreshCode}>
                  <img src={data?.data.code} alt="" />
                </div>
              </Col>
            </Row>
          </ProForm.Item>
        </LoginForm>
      </div>
      <Footer />
    </div>
  );
};

export default Login;
