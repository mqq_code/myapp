import type { Settings as LayoutSettings } from '@ant-design/pro-layout';
import { PageLoading } from '@ant-design/pro-layout';
import type { RunTimeLayoutConfig } from 'umi';
import { history, Link } from 'umi';
import RightContent from '@/components/RightContent';
import Footer from '@/components/Footer';
import { getUserInfo, getUserMenu } from '@/services/user';

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 *
 * */
export async function getInitialState(): Promise<{
  currentUser?: UserAPI.UserInfoRes;
  loading?: boolean;
  fetchUserInfo: () => Promise<UserAPI.UserInfoRes | undefined>;
}> {
  const fetchUserInfo = async () => {
    try {
      const res = await getUserInfo();
      return res.data;
    } catch (error) {
      return undefined;
    }
  };
  // 如果不是登录页面，执行
  if (history.location.pathname !== '/login') {
    const currentUser = await fetchUserInfo();
    return {
      fetchUserInfo,
      currentUser,
    };
  }
  return {
    fetchUserInfo,
  };
}

// ProLayout 支持的api https://procomponents.ant.design/components/layout
export const layout: RunTimeLayoutConfig = ({ initialState, setInitialState }) => {
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    waterMarkProps: {
      content: initialState?.currentUser?.username,
    },
    footerRender: () => <Footer />,
    onPageChange: (location) => {
      if (initialState?.currentUser && location?.pathname !== '/login') {
        // 跳转路由时判断是否有权限
        const exist = initialState?.currentUser?.permission.find(
          (v) => v.path === location?.pathname,
        );
        if (!exist) {
          history.replace('/403');
        }
      }
    },
    menuHeaderRender: undefined,
    menu: {
      params: initialState?.currentUser?.username,
      request: async (params) => {
        if (initialState?.currentUser) {
          // 获取左侧菜单列表
          const res = await getUserMenu();
          return res.data.list;
        }
        return [];
      },
    },
    // 自定义 403 页面
    // unAccessible: <div>unAccessible</div>,
    // 增加一个 loading 的状态
    childrenRender: (children, props) => {
      // if (initialState?.loading) return <PageLoading />;
      return <>{children}</>;
    },
  };
};
